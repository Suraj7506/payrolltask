import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [ //collection of objects
  {path:'auth',loadChildren:()=>import('../app/view/auth/auth.module').then(m=>m.AuthModule)},
  {path:'',loadChildren:()=>import('../app/view/main/main.module').then(m=>m.MainModule)},
  {path:'**',redirectTo:'page-not-found',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],      //forRoot creates a module that contains all the directives, the given routes, and the router service itself
  exports: [RouterModule]                       //forChild creates a module that contains all the directives and the given routes, but does not include the router service.
})
export class AppRoutingModule { 
}
