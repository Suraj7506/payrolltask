import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import { ToastrModule } from 'ngx-toastr';
import { Interceptor } from './core/interceptor/interceptor';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { CustomDatePipe } from './core/pipes/custom-date.pipe';
import { DeactivateGuard } from './core/auth/deactivate/deactivate.guard';

@NgModule({
  declarations: [
    AppComponent,
    CustomDatePipe,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    LoadingBarModule,
    LoadingBarRouterModule,
    LoadingBarHttpClientModule
  ],
  providers: [DeactivateGuard,Interceptor,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: Interceptor,
			multi: true
		},],
  bootstrap: [AppComponent]
})
export class AppModule { }
