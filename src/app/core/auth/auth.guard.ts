import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'         //providedIn: 'root' specifies that Angular should provide the service in the root injector.
})
export class AuthGuard implements CanActivate {
  constructor( private router: Router) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      if(localStorage.getItem('token'))
      {
        return true;
      }
      this.router.navigateByUrl('/auth/login');
      return false;
     
  }
  
}


//CanActivate - Decides if a route can be activated