import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
// import { environment } from 'src/environments/environment';
import { environment } from 'environments/environment';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
   CurrentUser = new Subject();
  
  constructor(private http: HttpClient) { }
    // Authentication/Authorization
    login(username: string, password: string): Observable<any> {
      return this.http.post<any>('api/account/authenticate', { username, password });
      //environment.appUrl+
  }

}
