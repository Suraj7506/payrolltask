import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AddTaskComponent } from 'app/view/main/add-task/add-task.component';

@Injectable({
  providedIn: 'root'
})
export class DeactivateGuard implements CanDeactivate <AddTaskComponent>{
  canDeactivate(component: AddTaskComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot | undefined): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    //return window.confirm('Do you want to leave this page?');
    return component.canExit();
  }
  
}

// The @Injectable() decorator defines a class as a service in Angular and allows Angular to inject it into a component as a dependency.
//CanDeactivate - Decides if a route can be deactivated.