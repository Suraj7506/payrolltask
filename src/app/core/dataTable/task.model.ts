export interface TaskElement {
    Title: string;
    
    LeadName: string;
    AssignedToUserName: string;
    CreateDate: string;
    TaskEndDate: string;
    Priority: string;
    TaskStatus: number;
  }