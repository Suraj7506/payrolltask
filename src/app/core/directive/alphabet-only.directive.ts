import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: 'input[appAlphabetOnly]'
})

export class AlphabetOnlyDirective {
//   key!: any;
  constructor(private _el:ElementRef){}
  @HostListener('input', ['$event']) onInputChange(event:any) {
    const initalValue = this._el.nativeElement.value;
        this._el.nativeElement.value = initalValue.replace(/[^a-z^A-Z ]*/g, '');
//         if (initalValue !== this._el.nativeElement.value) {
//             event.stopPropagation();
//         }
  }
}
