import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { ToastrServices } from "../toastr/toastr.service";
@Injectable({ providedIn: 'root' })
export class Interceptor implements HttpInterceptor {
    constructor(private router: Router, private toastr: ToastrServices) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (localStorage.getItem('token')) {
            request = request.clone({
                setHeaders: {
                    Authorization: `${localStorage.getItem('token')}`
                }
            });
        }
        return next.handle(request).pipe(
            tap(
                event => {
                    if (event instanceof HttpResponse) {
                        // http response status code
                        
                        if (event.body.status == 401) {
                            localStorage.removeItem('token');
                            localStorage.removeItem('CurrentUser');
                            this.router.navigate(['auth/login']);
                        }
                    }
                },
                error => {
                    console.error(error.status);
                    console.error(error.message);
                        this.toastr.showError('Something Went Wrong.');
                }
            )

        );
    }
}
