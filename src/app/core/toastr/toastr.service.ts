import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ToastrServices {

  toastConfig: any = {};
  constructor(private toastrService: ToastrService) {
    this.toastConfig = {
      enableHtml: true,
      timeOut: 1100,
      closeButton: true,
    };
  }

  showSuccess(message: string | undefined, title?: string | undefined) {
    this.toastrService.success(message, title, this.toastConfig);
  }

  showError(message: string | undefined, title?: string | undefined) {
    this.toastrService.error(message, title, this.toastConfig);
  }

  showInfo(message: string | undefined, title: string | undefined) {
    this.toastrService.info(message, title, this.toastConfig);
  }

  showWarning(message: string | undefined, title: string | undefined) {
    this.toastrService.warning(message, title, this.toastConfig);
  }
}
