import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { ReverseGuard } from '/app/core/reverse/reverse.guard';
import { ReverseGuard } from 'app/core/reverse/reverse.guard';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path:'',
    component:AuthComponent,
    canActivate:[ReverseGuard],
    children:[
      {
        path:'',
        redirectTo: 'login',
				pathMatch: 'full'
      },
      {
				path: 'login',
				component: LoginComponent,
			},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
