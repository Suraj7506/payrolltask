import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import { ToastrModule } from 'ngx-toastr';
// import { NumberOnlyDirective } from '/app/core/directive/number-only.directive';
import { NumberOnlyDirective } from 'app/core/directive/number-only.directive';
@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    NumberOnlyDirective
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    HttpClientModule,
    ToastrModule.forRoot()
  ],
  // providers: [Interceptor,
	// 	{
	// 		provide: HTTP_INTERCEPTORS,
	// 		useClass: Interceptor,
	// 		multi: true
	// 	},],
  exports:[AuthComponent]
})
export class AuthModule { }
