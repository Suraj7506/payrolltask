import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
// import { AuthService } from 'src/app/core/auth/auth.service';
import { AuthService } from 'app/core/auth/auth.service';
import { ToastrServices } from 'app/core/toastr/toastr.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginFormGroup!: FormGroup;

  constructor(private auth:AuthService,private router: Router,private route: ActivatedRoute,private toastr: ToastrServices) { }

  ngOnInit() {
    this.loginFormGroup = new FormGroup({
      username : new FormControl('', [Validators.required]),
      password : new FormControl('', [Validators.required])
    });
  }

  public checkError = (controlName: string, errorName: string) => {
    return this.loginFormGroup.controls[controlName].hasError(errorName);
  }

  onSubmit()
  {
    
    const controls = this.loginFormGroup.controls;
    const authData = {
			username: controls['username'].value,
			password: controls['password'].value
		};
    this.auth.login(authData.username, authData.password)
    .pipe(tap(res=>{
      if(res.success)
      {
        localStorage.setItem('token','Basic ' + btoa(authData.username + ':' + authData.password));
        this.auth.CurrentUser.next(res.userDetail.data.UserId);
        localStorage.setItem('CurrentUser',res.userDetail.data.UserId);
        this.router.navigateByUrl('/home');
        this.toastr.showSuccess('Your Login Successfully...','Welcome '+res.userDetail.data.Name)
      }
      else
      {
        this.toastr.showError(res.errormessage,'Error')
      }
    }))
    .subscribe();
  }
}

