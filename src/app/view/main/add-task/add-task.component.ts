import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs';
// import { TaskService } from 'src/app/core/task/task.service';
import { TaskService } from 'app/core/task/task.service';
// import { ToastrServices } from 'src/app/core/toastr/toastr.service';
import { ToastrServices } from 'app/core/toastr/toastr.service';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  @ViewChild('imageFileInput', { static: false }) imageFileInput!: ElementRef;
 public addTaskFormGroup!: FormGroup;
 imageExt: string = '';
 imageName: string = '';
 customerList!: any[];
 memberList: any = [];
 memberfilterArrayList: any = [];
 memberfilterForArrayList: any = [];
 viewLoading: boolean = false;
 userList:any=[];
 displayFileName: string = '';
 today = new Date();
 current = new Date();
 leadFilter: any;
 memberFilter: any;
 selectLength=0;
 userFilter: any;
 pageData = {
  From: 1,
  To: -1,
  Text: '',
}
  constructor(  public dialogRef: MatDialogRef<AddTaskComponent>,
  private chRef: ChangeDetectorRef,
  private toastrService: ToastrService,
  private taskService: TaskService,
  private toastr: ToastrServices,
  private formBuilder:FormBuilder
   ) {
    this.current.setDate(this.today.getDate()+1)
    }

  ngOnInit(): void {
    this.addTaskForm();
    this.getCustomerList();
    this.getMembersList(1, 100, '');
  }
  addTaskForm()
  {
    this.addTaskFormGroup = this.formBuilder.group({
      Title :['',[Validators.required,Validators.pattern('^[a-zA-Z ]*$')]],
      Description :['', Validators.required],
      Image : ['',[Validators.required]],
      LeadId :['',[Validators.required]],
       TaskEndDateDisplay: ['',[Validators.required]],
      TaskEndDate: [''],
      Priority :['',[Validators.required]],
      UserIds : ['',[Validators.required]],
      TaskOwners : ['',[Validators.required]]
    })

    this.addTaskFormGroup.controls['UserIds'].valueChanges.subscribe(
      res=> {
        this.selectLength = res.length;
      }
    )
  }
  public checkError = (controlName: string, errorName: string) => {
    return this.addTaskFormGroup.controls[controlName].hasError(errorName);
  }
  canExit() : boolean {
 
    if (confirm("Do you wish to Please confirm")) {
      this.dialogRef.close();
        return true
      } else {
        return false
      }
    }
  submit()
  {
    var datePipe = new DatePipe("en-US");
    var custDate = datePipe.transform(this.addTaskFormGroup.controls['TaskEndDateDisplay'].value, 'd MMM yyyy hh:mm a');
    this.addTaskFormGroup.controls['TaskEndDate'].setValue(custDate);

    this.memberfilterArrayList=this.addTaskFormGroup.controls['TaskOwners'].value
    for (let i = 0; i < this.memberfilterArrayList.length; i++) {
      var foundValue = this.memberList.find((obj: { UserId: number;Name:string })=>obj.UserId==this.memberfilterArrayList[i]);
      this.memberfilterForArrayList.push(foundValue);
    }
    this.viewLoading = true;
    this.taskService.addTask(this.addTaskFormGroup.value)
      .pipe(map(res => {
        if (res.Status == 200) {
          this.dialogRef.close({ res });
          this.viewLoading = false;
          this.toastr.showSuccess('Task Added Successfully...')
          this.chRef.detectChanges();
        }
        
      })).subscribe();
  }
  openFile() {
    this.imageFileInput.nativeElement.click();
  }
  removeFile() {
    this.imageFileInput.nativeElement.value = '';
    this.displayFileName = '';
    this.addTaskFormGroup.patchValue({
      Image: '',
    });
  }
  handleFileSelect(inputValue: any): void {
    if (inputValue.files[0] && inputValue.files[0].size < 2000000) {
      var file = inputValue.files[0];
      this.displayFileName = file.name;
      this.imageName = this.displayFileName.replace(/\\/g, "/");
      this.imageName = this.imageName.substring(0, this.imageName.lastIndexOf('.'));
      var reader = new FileReader();
      reader.onload = (e: any) => {
        var binaryData = e.target.result;
        var base64String = btoa(binaryData);
        var imagePath = base64String;
        this.addTaskFormGroup.patchValue({
          Image: imagePath
        });
      }
      reader.readAsBinaryString(file);
    } else if (inputValue.files[0] && inputValue.files[0].size > 2000000) {
      this.toastrService.error('File size is greater than 2MB');
      this.chRef.detectChanges();
    }
  }

  searchManager(searchText: string, type: string) {
    if (type == 'lead') {
      if (searchText != '') {
        this.leadFilter = this.customerList.filter((item: { LeadName: { toString: () => string; }; }) => {
          if (item.LeadName.toString().toLowerCase().indexOf(searchText.toLowerCase()) !== -1) {
            return true;
          }
          return false;
        }
        );
      }
      else {
        this.leadFilter = this.customerList;
      }
    } 
    else if(type == 'user')
    {
      if (searchText != '') {
        this.userFilter = this.userList.filter((item: { Name: { toString: () => string; }; }) => {
          if (item.Name.toString().toLowerCase().indexOf(searchText.toLowerCase()) !== -1) {
            return true;
          }
          return false;
        }
        );
      }
      else {
        this.userFilter = this.userList;
      }
    }
    else if(type == 'member')
    {
      if (searchText != '') {
        this.memberFilter = this.memberList.filter((item: { Name: { toString: () => string; }; }) => {
          if (item.Name.toString().toLowerCase().indexOf(searchText.toLowerCase()) !== -1) {
            return true;
          }
          return false;
        }
        );
      }
      else {
        this.memberFilter = this.memberList;
      }
    }
  }
  getCustomerList() {
    this.taskService.getCustomerList(this.pageData)
      .pipe(map(res => {
        this.customerList = res.data.Leads;
        this.leadFilter = this.customerList;
      }

      ))
      .subscribe()
  }
  getMembersList(from: number, to: number, text: string) {
    this.taskService.getCompanyMembers(from, to, text)
      .pipe(
        map(members => {
          this.memberList=members.data.Members;
          this.userList=members.data.Members;
          this.memberFilter=this.memberList;
          this.userFilter=this.userList;
        })
      ).subscribe();
  }
}
