import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { AuthGuard } from 'src/app/core/auth/auth.guard';
import { AuthGuard } from 'app/core/auth/auth.guard';
import { AddTaskComponent } from './add-task/add-task.component';
import { MainComponent } from './main.component';

const routes: Routes = [	
  {
    path:'',
    component:MainComponent,
    canActivate:[AuthGuard],
    children:[
      {path:'home',component:MainComponent},
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: '**', redirectTo: 'home', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
