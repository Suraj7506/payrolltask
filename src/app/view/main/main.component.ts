import { Router } from '@angular/router';
import { Component, ViewChild,OnInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { HttpClient } from '@angular/common/http';
import { map} from 'rxjs/operators';
import { AuthService } from 'app/core/auth/auth.service';
// import { TaskService } from 'src/app/core/task/task.service';
import { TaskService } from 'app/core/task/task.service';
// import { TaskElement } from 'src/app/core/dataTable/task.model';
import { TaskElement } from 'app/core/dataTable/task.model';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { AddTaskComponent } from './add-task/add-task.component';
// import { ToastrServices } from 'src/app/core/toastr/toastr.service';
import { ToastrServices } from 'app/core/toastr/toastr.service';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ELEMENT_DATA!:TaskElement[];
  displayedColumns: string[] = ['Title', 'LeadName', 'AssignedToUserName', 'CreateDate','TaskEndDate','Priority','TaskStatus','Action'];
  currentUser=localStorage.getItem('CurrentUser');
  dataSource = new MatTableDataSource<TaskElement>(this.ELEMENT_DATA);
  constructor( private router: Router,private http: HttpClient,private auth:AuthService,private taskService: TaskService,public dialog: MatDialog,private toastr: ToastrServices) { }

  ngOnInit() {
   
    this.getTaskTable();
  }

  logout()
  {
    localStorage.removeItem('token');
    localStorage.removeItem('CurrentUser');
    this.router.navigate(['/auth/login']);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getTaskTable()
  {
    this.taskService.findMyTask(1, -1, '', Number(this.currentUser), false,'','','','','','','').subscribe(res=>this.dataSource.data=res.data.TaskList as TaskElement[]);
  }

  applyFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addTask()
  {
    const dialogRef = this.dialog.open(AddTaskComponent,{disableClose: true , width: '650px'});
    dialogRef.afterClosed().subscribe(res => {
      if (!res)
        return
      this.getTaskTable();
    })
    
  }

  deleteTask(id: any)
  {
   if(window.confirm('Do you want to Delete Task?'))
   {
    this.taskService.deleteTask(id)
    .pipe(map(res => {
      if (res.Status == 200) {
        this.toastr.showSuccess('Task Deleted Successfully...')
        this.getTaskTable();
      }
    }))
    .subscribe();
  }
}
}
