import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { AddTaskComponent } from './add-task/add-task.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { AlphabetOnlyDirective } from 'app/core/directive/alphabet-only.directive';

@NgModule({
  declarations: [
    MainComponent,
    AddTaskComponent,
    AlphabetOnlyDirective
  ],
  imports: [
    CommonModule,
    MatDividerModule,
    MainRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    LoadingBarModule,
    HttpClientModule,
    LoadingBarRouterModule,
    LoadingBarHttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule, 
  ],
  // providers: [
  //   Interceptor,
	// 	{
	// 		provide: HTTP_INTERCEPTORS,
	// 		useClass: Interceptor,
	// 		multi: true
	// 	},],
  exports:[
    MainComponent,
    AlphabetOnlyDirective
  ],
  entryComponents:[
    AddTaskComponent
  ]
})
export class MainModule { }
